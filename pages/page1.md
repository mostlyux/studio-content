---
fields:
  icons:
    type: checkbox
    label: Icons
    value:
      - Save
      - Settings
    options:
      - Settings
      - Save
      - Search
---

# Page 1.3

This is mark down, so you can do markdown things.

- One
- Two
- Three
  - 3.5
-Four

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nisl diam, fringilla tristique turpis et, fermentum vehicula augue. In hac habitasse platea dictumst. Morbi sit amet lacinia mi. In hac habitasse platea dictumst. Cras feugiat elit at laoreet cursus. Fusce malesuada quam ut massa facilisis, non mattis ipsum molestie. Praesent in volutpat nibh, et suscipit est. Cras leo sapien, elementum ac est vitae, faucibus aliquet urna. Quisque cursus neque sit amet diam semper pretium. Sed quis nisi augue.

[Link](www.example.com)


