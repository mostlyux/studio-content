---
fields:
  title:
    type: input
    label: Title
    value: Component 2
  theme:
    type: select
    label: Theme
    value: Dark
    options:
      - Default
      - Dark
  align:
    type: radio
    label: Align
    value: Center
    options:
      - Left
      - Center
      - Right
---
And...this is an example of another component.


