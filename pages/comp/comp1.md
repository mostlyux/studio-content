---
fields:
  title:
    type: input
    label: Title
    value: Component 1
  theme:
    type: select
    label: Theme
    value: Dark
    options:
      - Default
      - Dark
  align:
    type: radio
    label: Align
    value: Center
    options:
      - Left
      - Center
      - Right
---

This is an example of a component. 
